const { literal } = require("sequelize");
const Categoria = require("../models/Categoria");
const Producto = require("../models/Producto");



class ProductosController {
    
    static async crearProductos(req, res){

        try {
            const { codigo, nombre, categoria_id, cantidad, estado } = req.body
            const model = new Producto()
            model.codigo = codigo
            model.nombre = nombre
            model.categoria_id = categoria_id
            model.cantidad = cantidad
            model.estado = estado
            await model.save()

            return res.status(201).json({ message: 'guardado correctamente', data: model })

        } catch (error) {
            return res.status(400).json({ message: 'error al guardar', error: error })
        }
    }

    static async editarProductos(req, res){

        try {
            const { id_producto } = req.params
            const { codigo, nombre, categoria_id, cantidad, estadoo } = req.body
            
            const model = await Producto.findByPk(id_producto)
            model.codigo = codigo
            model.nombre = nombre
            model.categoria_id = categoria_id
            model.cantidad = cantidad
            model.estadoo = estadoo
            await model.save()

            return res.status(200).json({ message: 'guardado correctamente', data: model })

        } catch (error) {
            return res.status(400).json({ message: 'error al guardar', error: error })
        }
    }

    static async listarProductos(req, res){

        try {
            let data = await Producto.findAll({
                attributes:[
                    'id',
                    'codigo',
                    'nombre',
                    'cantidad',
                    'estado',
                    [literal('(SELECT nombre FROM categorias AS c WHERE c.id = categoria_id)'), 'categoria']
                ]
            })

            return res.status(200).json({ message: 'consultado correctamente', data: data })

        } catch (error) {
            return res.status(400).json({ message: 'error al listar', error: error })
        }
    }

    static async eliminarProductos(req, res){

        try {
            const { id_producto } = req.params
            Producto.destroy({
                where: {
                   id: id_producto 
                }
             })

            return res.status(200).json({ message: 'eliminado correctamente' })

        } catch (error) {
            return res.status(400).json({ message: 'error al eliminar', error: error })
        }
    }

    static async listarCategorias(req, res){

        try {
            let data = await Categoria.findAll({
                attributes:[
                    'id',
                    'nombre'
                ]
            })

            return res.status(200).json({ message: 'consultado correctamente', data: data })

        } catch (error) {
            return res.status(400).json({ message: 'error al eliminar', error: error })
        }
    }

    
    
}

module.exports = ProductosController;