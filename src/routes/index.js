const express = require('express');
const ProductosController = require('../controllers/ProductosController');
const router = express.Router();


router.post('/crear-productos', ProductosController.crearProductos)
router.put('/editar-productos/:id_producto', ProductosController.editarProductos)
router.get('/listar-productos', ProductosController.listarProductos)
router.delete('/eliminar-productos/:id_producto', ProductosController.eliminarProductos)
router.get('/listar-categorias', ProductosController.listarCategorias)


module.exports = router;