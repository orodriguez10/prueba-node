const { Model, DataTypes } = require('sequelize');
const { sequelize } = require('../config/db');


class Producto extends Model { }

Producto.init({
    id:{
        autoIncrement: true,
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        primaryKey: true
    },
    codigo: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
    },
    nombre: {
        type: DataTypes.STRING(100),
        allowNull: true,
    },
    categoria_id: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
            model: 'categorias',
            key: 'id'
        },
    },
    cantidad: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: true,
    },
    estado: {
        type: DataTypes.TINYINT.UNSIGNED,
        allowNull: false,
        defaultValue: 1,
        comment: "1=activo 2=inactivo",
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
        field: 'created_at'
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
        field: 'updated_at'
    },
},
{
    sequelize: sequelize,
    tableName: 'productos',
    timestamps: true,
})

module.exports = Producto;