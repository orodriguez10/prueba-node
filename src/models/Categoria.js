const { Model, DataTypes } = require('sequelize');
const { sequelize } = require('../config/db');

class Categoria extends Model { }

Categoria.init({
    id:{
        autoIncrement: true,
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        primaryKey: true
    },
    nombre: {
        type: DataTypes.STRING(100),
        allowNull: true,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
        field: 'created_at'
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
        field: 'updated_at'
    },
},
{
    sequelize: sequelize,
    tableName: 'categorias',
    timestamps: true,
})

module.exports = Categoria;