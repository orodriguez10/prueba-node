const dotenv = require('dotenv');
const path = require('path');

const envVars = dotenv.config({ path: path.join(__dirname, '../../.env') }).parsed;


const config = {
    port: envVars.PORT,

    db:{    
        username: envVars.DB_USERNAME,
        password: envVars.DB_PASSWORD,
        database: envVars.DB_DATABASE,
        host: envVars.DB_HOST
    }

}

module.exports = {config}

