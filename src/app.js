const express = require('express')
const app = express()
const { config }  = require('./config/config');



const {sequelize, sequelizeCde} = require('./config/db');
// const Avatares = require('./models/Avatares')

//ROUTES
app.use(express.json());
app.use(express.urlencoded({extended:false}));

// app.use(cors());
// app.options('*', cors());

const routes = require('./routes/index');

app.use('/api',routes)


app.listen(config.port, () => {
  console.log(`listening at http://localhost:${config.port}`)

  //conectar a base de datos

  // sequelize.sync({force: false}).then(()=>{
  //     console.log("conectado a la base de datos");
  // }).catch(error =>{
  //     console.log("se ha producido un error");
  // })
})
